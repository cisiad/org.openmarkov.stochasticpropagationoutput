package org.openmarkov.stochasticPropagationOutput;//package org.openmarkov.inference.likelihoodWeighting;
//
//import org.apache.poi.ss.usermodel.*;
//import org.apache.poi.ss.util.CellRangeAddress;
//import org.apache.poi.xssf.usermodel.*;
//import org.openmarkov.core.exception.InvalidStateException;
//import org.openmarkov.core.inference.annotation.InferenceAnnotation;
//import org.openmarkov.core.model.network.EvidenceCase;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import sun.invoke.empty.Empty;
//
//
//import java.awt.*;
//import java.awt.Color;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.lang.reflect.Method;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//
//
//
//public class WriteToXlsx {
//
//    // Constructor
//    public WriteToXlsx(StochasticPropagationAlgorithm algorithm) throws InvalidStateException {
//
//        // COLORS
//        final Color CHANCE_NODE_COLOR = new Color(251, 249, 153);
//        final Color FINDING_NODE_COLOR = Color.GRAY;
//
//        /**
//         * Colors of the table headers
//         */
//        final Color HEADER_COLOR = new java.awt.Color(251, 210, 153);
//        final Color HEADER_COLOR_2 = new java.awt.Color(255, 220, 180);
//
//        /**
//         * Name for the spreadsheet created.
//         */
//        String FILE_NAME = "/home/iago/untitled.xlsx"; // TODO Generalize save route - JFileChooser
//
//        // GETTING DATA FROM THE ALGORITHM
//        // Samples
//        try {
//            double[][] sampleStorage = algorithm.getSampleStorage();
//            if (sampleStorage.length == 0) {
//                throw new NullPointerException();
//            }
//        } catch(NullPointerException e) {
//            System.out.println("There is no database"); // ¿Como expulsar esta información?
//        }
//
//        // Potentials
//        HashMap<Variable,TablePotential> potentialsStorage = algorithm.getPotentialsStorage();
//
//        // Variables
//        List<Variable> sampledVariables = algorithm.getVariablesToSample(); // Extract the sampled variables
//        // Their number of states (with the maximum I can get the columns of the second sheet in Excel)
//        ArrayList<Integer> nStates = new ArrayList<>(); // and its possible states
//        int maxNStates = 0;
//        for (Variable variable : sampledVariables) {
//            if (variable.getNumStates() > maxNStates) {
//                maxNStates = variable.getNumStates();
//            }
//        }
//
//        // Evidence variables
//        EvidenceCase evidenceCase = algorithm.getPostResolutionEvidence();
//        List<Variable> evidenceVariables = evidenceCase.getVariables();
//
//        // Set the algorithm in the name of the file
//        // TODO Use localization string for samples
//        FILE_NAME = "~/" + algorithm.getNetName() + " " + algorithm.name + " samples.xlsx";
//        // TODO Test if ~ works
//
//
//        // CREATING THE WORKBOOK
//
//        XSSFWorkbook workbook = new XSSFWorkbook();
//
//
//
//        // Create styles
//
//        // Font with big emphasis for the next styles
//        XSSFFont bigBoldArial = workbook.createFont();
//        bigBoldArial.setFontName("Arial");
//        bigBoldArial.setFontHeightInPoints((short) 11);
//        bigBoldArial.setBold(true);
//
//        // Header of table
//        XSSFCellStyle topTable = workbook.createCellStyle();
//
//        topTable.setFillForegroundColor(new XSSFColor(HEADER_COLOR));
//        topTable.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        topTable.setAlignment(HorizontalAlignment.LEFT);
//        topTable.setFont(bigBoldArial);
//
//
//        // Header of table centered
//        XSSFCellStyle centeredTopTable = workbook.createCellStyle();
//
//        centeredTopTable.setFillForegroundColor(new XSSFColor(HEADER_COLOR_2));
//        centeredTopTable.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        centeredTopTable.setAlignment(HorizontalAlignment.CENTER);
//        centeredTopTable.setFont(bigBoldArial);
//
//        // Chance node
//        XSSFCellStyle chanceNode = workbook.createCellStyle();
//
//        chanceNode.setFillForegroundColor(new XSSFColor(CHANCE_NODE_COLOR));
//        chanceNode.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        chanceNode.setFont(bigBoldArial);
//
//
//
//        // Font with big emphasis and white for contrast
//        XSSFFont bigWhiteArial = workbook.createFont();
//        bigWhiteArial.setFontName("Arial");
//        bigWhiteArial.setFontHeightInPoints((short) 11);
//        bigWhiteArial.setBold(true);
//        bigWhiteArial.setColor(new XSSFColor(new Color(255, 255, 255)));
//
//        // Node with finding
//        XSSFCellStyle findingNode = workbook.createCellStyle();
//
//        findingNode.setFillForegroundColor(new XSSFColor(FINDING_NODE_COLOR));
//        findingNode.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        findingNode.setFont(bigWhiteArial);
//
//        // No more than four decimal places
//        XSSFCellStyle smartDecimals = workbook.createCellStyle();
//        smartDecimals.setDataFormat(workbook.createDataFormat().getFormat("0.####"));
//
//        // No more than four decimal places
//        XSSFCellStyle smartFewDecimals = workbook.createCellStyle();
//        smartFewDecimals.setDataFormat(workbook.createDataFormat().getFormat("0.##"));
//
//
//
//        // Font with a bit less emphasis
//        XSSFFont boldArial = workbook.createFont();
//        boldArial.setFontName("Arial");
//        boldArial.setFontHeightInPoints((short) 10);
//        boldArial.setBold(true);
//
//        // Subheader of table
//        XSSFCellStyle subTopTable = workbook.createCellStyle();
//
//        subTopTable.setFont(boldArial);
//
//
//        // SHEET OF SAMPLES
//        // TODO Localize every string below
//        XSSFSheet samples = workbook.createSheet("Samples");
//
//        // TITLE ROW
//        Row titlesRow = samples.createRow(0);
//        titlesRow.createCell(0).setCellValue("Sample Number");
//        for (int colNum = 1; colNum < sampledVariables.size() + 1; colNum++) { // Names of variables
//            titlesRow.createCell(colNum).setCellValue(sampledVariables.get(colNum - 1).toString());
//        }
//        titlesRow.createCell(sampledVariables.size() + 1).setCellValue("Weight");
//
//        // Apply header style
//        for (int colNum = 0; colNum < sampledVariables.size() + 2; colNum++) {
//            titlesRow.getCell(colNum).setCellStyle(topTable);
//        }
//
//        // SAMPLES ROWS
//        int rowNum;
//        int limit = 1000;
//        if (sampleStorage.length < 1000) {
//            limit = sampleStorage.length;
//        }
//        for (rowNum = 1; rowNum < limit + 1; rowNum++) {
//
//            Row sampleRow = samples.createRow(rowNum);
//            sampleRow.createCell(0).setCellValue(rowNum);
//            for (int colNum = 1; colNum < sampledVariables.size() + 2; colNum++) { // Sampled states
//                sampleRow.createCell(colNum).setCellValue(sampleStorage[rowNum - 1][colNum - 1]);
//            }
//
//            // Extract weight to create specific color
//            double weight = sampleStorage[rowNum - 1][sampledVariables.size()];
//            Color weightColor;
//            if (weight == 0) {
//                weightColor = new Color(255,120,120);
//            } else {
//                weightColor = new Color(120,255,120);
//            }
//
//            // Create the specific style to convey graphically the weight
//            XSSFCellStyle coloredSample = workbook.createCellStyle(); // The style must be reinitialized
//            coloredSample.setFillForegroundColor(new XSSFColor(weightColor));
//            coloredSample.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//            // and set it.
//            for (int colNum = 1; colNum < sampledVariables.size() + 2; colNum++) {
//                sampleRow.getCell(colNum).setCellStyle(coloredSample);
//            }
//        }
//        if (rowNum == 1001) {
//            Row sampleRow = samples.createRow(rowNum);
//            sampleRow.createCell(0).setCellValue((sampleStorage.length - 1000) + " samples more");
//        }
//
//        // Auto-size columns of first sheet
//        for (int colNum = 0; colNum < sampledVariables.size() + 2; colNum++) {
//            samples.autoSizeColumn(colNum);
//        }
//
//
//        // SHEET OF GENERAL STATS
//
//        XSSFSheet generalStats = workbook.createSheet("General stats");
//
//        // ROWS OF ALGORITHM DATA
//
//        generalStats.createRow(0).createCell(0).setCellValue("Algorithm");
//        generalStats.getRow(0).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(0).createCell(1).setCellValue(algorithm.name);
//        generalStats.getRow(0).getCell(1).setCellStyle(subTopTable);
//
//        generalStats.createRow(1).createCell(0).setCellValue("Computing time (ms)");
//        generalStats.getRow(1).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(1).createCell(1).
//                setCellValue(algorithm.getAlgorithmExecutionTime());
//        generalStats.getRow(1).getCell(1).setCellStyle(smartFewDecimals);
//
//        generalStats.createRow(2).createCell(0).setCellValue("Computing time per sample (ms)");
//        generalStats.getRow(2).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(2).createCell(1).
//                setCellValue(algorithm.getAlgorithmExecutionTime()/algorithm.getSampleSize());
//        generalStats.getRow(2).getCell(1).setCellStyle(smartDecimals);
//
//        generalStats.createRow(3).createCell(0).setCellValue("Nº of samples");
//        generalStats.getRow(3).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(3).createCell(1).setCellValue(algorithm.getSampleSize());
//
//        generalStats.createRow(4).createCell(0).setCellValue("Non-null samples");
//        generalStats.getRow(4).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(4).createCell(1).setCellValue(algorithm.getPositiveSampleCount());
//
//        generalStats.createRow(5).createCell(0).setCellValue("Accumulated weight");
//        generalStats.getRow(5).getCell(0).setCellStyle(topTable);
//        generalStats.getRow(5).createCell(1).setCellValue(algorithm.getAccumulatedWeight());
//        generalStats.getRow(5).getCell(1).setCellStyle(smartFewDecimals);
//
//        // Empty row
//
//        rowNum = 7; // Rows written until here
//
//        // ROW OF TITLES
//        titlesRow = generalStats.createRow(rowNum);
//        titlesRow.createCell(0).setCellValue("Variable");
//
//        // Big long cell for state names
//        generalStats.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, maxNStates));
//        titlesRow.createCell(1).setCellValue("States");
//        // Set header style
//        for (int colNum = 0; colNum < 2; colNum++) {
//            titlesRow.getCell(colNum).setCellStyle(centeredTopTable);
//        }
//
//        rowNum++ ;
//
//        // Write about evidence variables that have not been sampled
//        for (int variablePosition = 0; variablePosition < evidenceVariables.size(); variablePosition++) {
//
//            Variable evidenceVariable = evidenceVariables.get(variablePosition);
//
//            if (sampledVariables.indexOf(evidenceVariable) < 0) {
//
//                // ROW OF NAMES
//                Row namesRow = generalStats.createRow(rowNum);
//                // Write the name of the variable
//                Cell variableName = namesRow.createCell(0);
//                variableName.setCellValue(evidenceVariable.toString());
//                variableName.setCellStyle(findingNode); // The color marks it as a finding
//
//                // and then the names of their states
//                for (int colNum = 1; colNum < evidenceVariable.getNumStates() + 1; colNum++) {
//                    Cell stateName = namesRow.createCell(colNum);
//                    stateName.setCellValue(evidenceVariable.getStateName(colNum - 1));
//                    stateName.setCellStyle(findingNode);
//                }
//
//                // ROW OF OCCURRENCES
//                // Write non-sampled for them
//                Row ocurrencesRow = generalStats.createRow(rowNum + 1);
//                Cell nSamples = ocurrencesRow.createCell(0);
//                nSamples.setCellValue("Ocurrences");
//                nSamples.setCellStyle(subTopTable)
//                ;
//                // Write non-sampled
//                for (int colNum = 1; colNum < evidenceVariable.getNumStates() + 1; colNum++) {
//                    ocurrencesRow.createCell(colNum)
//                            .setCellValue("Non sampled");
//                }
//
//                // ROW OF APPROXIMATE PROBABILITIES
//                Row approxProbsRow = generalStats.createRow(rowNum + 2);
//                Cell approximatedProbs = approxProbsRow.createCell(0);
//                approximatedProbs.setCellValue("Approximated probability");
//                approximatedProbs.setCellStyle(subTopTable);
//                // Write the finding
//                for (int colNum = 1; colNum < evidenceVariable.getNumStates() + 1; colNum++) {
//                    int value = 0;
//                    if (evidenceCase.getState(evidenceVariable) == colNum - 1) {
//                        value = 1;
//                    }
//                    approxProbsRow.createCell(colNum).setCellValue(value);
//                }
//
//                // ROW OF EXACT PROBABILITIES
//                Row exactProbsRow = generalStats.createRow(rowNum + 3);
//                Cell exactProbs = exactProbsRow.createCell(0);
//                exactProbs.setCellValue("Exact probability");
//                exactProbs.setCellStyle(subTopTable);
//                // Write the finding
//                for (int colNum = 1; colNum < evidenceVariable.getNumStates() + 1; colNum++) {
//                    int value = 0;
//                    if (evidenceCase.getState(evidenceVariable) == colNum - 1) {
//                        value = 1;
//                    }
//                    exactProbsRow.createCell(colNum).setCellValue(value);
//                }
//
//                // EMPTY ROW
//                rowNum += 5;
//            }
//        }
//
//        rowNum += 1; // Another empty row for clarity.
//
//        // Position of the variable in the list. It points to the columns of the matrix of samples (sampleStorage)
//        // where its sampled states are located.
//        for (int variablePosition = 0; variablePosition < sampledVariables.size(); variablePosition++) {
//
//            Variable variableToWrite = sampledVariables.get(variablePosition);
//
//            // ROW OF NAMES
//            Row namesRow = generalStats.createRow(rowNum);
//            // Write the name of the variable
//            Cell variableName = namesRow.createCell(0);
//            variableName.setCellValue(sampledVariables.get(variablePosition).toString());
//
//            // Then write the names of its states
//            for (int colNum = 1; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                Cell stateName = namesRow.createCell(colNum);
//                stateName.setCellValue(variableToWrite.getStateName(colNum - 1));
//                stateName.setCellStyle(subTopTable);
//            }
//
//            // In logical sampling, evidence variables are samples. If so, mark them as findings.
//            if (evidenceVariables.indexOf(variableToWrite) >= 0) {
//                for (int colNum = 0; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                    namesRow.getCell(colNum).setCellStyle(findingNode);
//                }
//            } else {
//                for (int colNum = 0; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                    namesRow.getCell(colNum).setCellStyle(chanceNode);
//                }
//            }
//
//            // ROW OF OCCURRENCES
//            Row occurrencesRows = generalStats.createRow(rowNum + 1);
//            // Write the title of the row: nº of samples
//            Cell nSamples = occurrencesRows.createCell(0);
//            nSamples.setCellValue("Occurrences");
//            nSamples.setCellStyle(subTopTable);
//            // Write how many times each state has been sampled
//            for (int colNum = 1; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                occurrencesRows.createCell(colNum)
//                        .setCellValue(getStateOccurrences(sampleStorage, variablePosition,colNum - 1));
//            }
//
//            // ROW OF APPROXIMATE PROBABILITIES
//            Row approxProbsRow = generalStats.createRow(rowNum + 2);
//            // Write the title of the row: Approximated probability
//            Cell approximatedProbs = approxProbsRow.createCell(0);
//            approximatedProbs.setCellValue("Approximated probability");
//            approximatedProbs.setCellStyle(subTopTable);
//            // Write how many times each state has been sampled
//            for (int colNum = 1; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                approxProbsRow.createCell(colNum)
//                        .setCellValue(potentialsStorage.get(variableToWrite).getValues()[colNum - 1]);
//                approxProbsRow.getCell(colNum).setCellStyle(smartDecimals);
//            }
//
//            // ROW OF EXACT PROBABILITIES
//            // TODO Implement Hugin
//            Row exactProbsRow = generalStats.createRow(rowNum + 3);
//            // Write the title of the row: Approximated probability
//            Cell exactProbs = exactProbsRow.createCell(0);
//            exactProbs.setCellValue("Exact probability");
//            exactProbs.setCellStyle(subTopTable);
//            // Write how many times each state has been sampled
//            for (int colNum = 1; colNum < variableToWrite.getNumStates() + 1; colNum++) {
//                exactProbsRow.createCell(colNum)
//                        .setCellValue(potentialsStorage.get(variableToWrite).getValues()[colNum - 1]);
//                exactProbsRow.getCell(colNum).setCellStyle(smartDecimals);
//            }
//
//
//            // EMPTY ROW
//            rowNum += 5; // Go to the next variable (the for loop runs through rows five by five)
//
//        }
//
//        // Auto-size columns of second sheet (+ 1 in the for limit just in case)
//        for (int colNum = 0; colNum < maxNStates + 2 + 1; colNum++) {
//            generalStats.autoSizeColumn(colNum);
//        }
//
//
//
//
//
//        // Output the file
//        try {
//            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
//            workbook.write(outputStream);
//            // remember to close
//            outputStream.close();
//            // workbook.close(); is only valid in apache.poi 3.17
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // TODO Localize
//        System.out.println("An Excel file with measured samples has been saved at " + FILE_NAME);
//    }
//
//    /**
//     * In a matrix ({@paramref storage}) that has samples as rows and variables as columns, take the variable in
//     * {@paramref variablePosition} and return how many samples have the state of index {@paramref stateIndex} inside
//     * of it.
//     * @param storage double[][] a matrix with samples in the rows and variables in the columns.
//     * @param variablePosition the column in the matrix which represents the variable you want.
//     * @param stateIndex the index of the sampled state you are accumulating.
//     * @return how many samples include the state of index {@paramref stateIndex} inside
//     * of {@paramref storage}.
//     * @throws InvalidStateException
//     */
//    private int getStateOccurrences(double[][] storage, int variablePosition, double stateIndex)
//            throws InvalidStateException {
//
//        int sum = 0;
//        for (int sampleNum = 0; sampleNum < storage.length; sampleNum++) {
//            if (storage[sampleNum][variablePosition] == stateIndex) {
//                sum++;
//            }
//        }
//        return sum;
//    }
//
//}
