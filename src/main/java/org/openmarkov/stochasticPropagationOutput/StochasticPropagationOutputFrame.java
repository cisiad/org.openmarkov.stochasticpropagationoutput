/*
 * Copyright (c) CISIAD, UNED, Spain,  2018. Licensed under the GPLv3 licence
 * Unless required by applicable law or agreed to in writing,
 * this code is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.stochasticPropagationOutput;


import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.ProbNet;

import org.openmarkov.gui.localize.StringDatabase;
import org.openmarkov.gui.plugin.ToolPlugin;
import org.openmarkov.gui.window.MainPanel;
import org.openmarkov.inference.likelihoodWeighting.*;

import javax.swing.*;

/**
 * @author iagoparis
 */
@ToolPlugin(name = "StochasticPropagationOutput", command = "Tools.StochasticPropagationOutput") public class StochasticPropagationOutputFrame
		extends JFrame {

	/**
	 * Constructor. initialises the instance.
	 *
	 * @param owner window that owns the dialog.
	 */
	public StochasticPropagationOutputFrame(JFrame owner) {
		super();
		ProbNet probNet = MainPanel.getUniqueInstance().getMainPanelListenerAssistant().getCurrentNetworkPanel()
				.getProbNet();
		EvidenceCase preResolutionEvidence = MainPanel.getUniqueInstance().
				getMainPanelMenuAssistant().getCurrentNetworkPanel().getEditorPanel().getPreResolutionEvidence();

	}
}
