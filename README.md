## Stochastic propagation algorithm output
> Package name: _org.openmarkov.stochasticpropagationoutput_

Plugin that allows Excel printing of Stochastic algorithms data. Likelihood weighting and logic sampling as for now (May 11, 2018).